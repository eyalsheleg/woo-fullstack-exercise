import { Candidate } from './../models/candidate';
import { Injectable } from '@angular/core';
import { Process } from '../models/process';
import { Position } from '../models/position';
import { Location } from '../models/location';
import { ProcessStatus } from '../enums/process-status.enum';
import { Tech } from '../models/tech';


@Injectable({
  providedIn: 'root'
})
export class CandidateService {
  public locations = [
    new Location(1, 'Jerusalem'),
    new Location(2, 'Tel-Aviv'),
    new Location(3, 'Herzelia'),
    new Location(4, 'Raanana'),
    new Location(5, 'Beer Sheva'),
  ];
  public techs = [
    new Tech(1, 'Angular'),
    new Tech(2, 'Java'),
    new Tech(3, 'NodJs'),
    new Tech(4, 'ReactJs'),
    new Tech(5, 'C#'),
  ];

  constructor(private candidate: Candidate) { }

  public getDetails(): Candidate {
    this.candidate.id = 2;
    this.candidate.name = 'Eyal';
    this.candidate.salary = 24000;
    this.candidate.locations = [this.locations[2], this.locations[1]];
    this.candidate.skills = [this.techs[2], this.techs[1], this.techs[3]];
    this.candidate.processes = [
      new Process(1,
        new Position(1, 'Full Stack Engineer', 'Google', [1, 2, 3], 22000, this.locations[0], [this.techs[0], this.techs[1], this.techs[2]]),
        ProcessStatus.ACCEPTED, '3 days ago', 75, 35),
      new Process(2,
        new Position(2, 'Full Stack Engineer', 'Facebook', [1, 2, 3], 20000, this.locations[2], [this.techs[4], this.techs[3]]),
        ProcessStatus.REJECTED, '1 day ago', 90, 55),
      new Process(3,
        new Position(3, 'Full Stack Engineer', 'Woo.io', [1, 2, 3], 19000, this.locations[1], [this.techs[3], this.techs[4], this.techs[1], this.techs[2], this.techs[0]]),
        ProcessStatus.NEW, '1 week ago', 40, 60),
      new Process(1,
        new Position(1, 'Full Stack Engineer', 'Google', [1, 2, 3], 22000, this.locations[0], [this.techs[0], this.techs[1], this.techs[2]]),
        ProcessStatus.ACCEPTED, '3 days ago', 75, 35),
      new Process(2,
        new Position(2, 'Full Stack Engineer', 'Facebook', [1, 2, 3], 20000, this.locations[2], [this.techs[4], this.techs[3]]),
        ProcessStatus.REJECTED, '1 day ago', 90, 55),
      new Process(3,
        new Position(3, 'Full Stack Engineer', 'Woo.io', [1, 2, 3], 19000, this.locations[1], [this.techs[3], this.techs[4], this.techs[1], this.techs[2], this.techs[0]]),
        ProcessStatus.NEW, '1 week ago', 40, 60),
    ];

    this.countProcesses();

    return this.candidate;
  }

  public getStatusText(process) {
    switch (process.status) {
      case ProcessStatus.NEW:
        return 'New';
        break;
      case ProcessStatus.ACCEPTED:
        return 'Accepted';
        break;
      case ProcessStatus.REJECTED:
        return 'Rejected';
        break;
    }
  }

  public getProccesses(type) {
    const processes: Process[] = [];
    this.candidate.processes.forEach(processesItem => {
      if (!type || processesItem.status === type) {
        processes.push(processesItem);
      }
    });

    return processes;
  }

  public countProcesses() {
    this.candidate.processesAcceptedCount = 0;
    this.candidate.processesNewCount = 0;
    this.candidate.processesRejectedCount = 0;
    this.candidate.processesAllCount = 0;
    this.candidate.processes.forEach(process => {
      this.candidate.processesAllCount++;
      switch (process.status) {
        case ProcessStatus.ACCEPTED:
          this.candidate.processesAcceptedCount++;
          break;
        case ProcessStatus.REJECTED:
          this.candidate.processesRejectedCount++;
          break;
        case ProcessStatus.NEW:
          this.candidate.processesNewCount++;
          break;
      }
    });
  }
}
