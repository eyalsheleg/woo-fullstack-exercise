import { TestBed } from '@angular/core/testing';

import { CandidateService } from './candidate.service';
import { Candidate } from '../models/candidate';

describe('CandidateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [Candidate]
  }));

  it('should be created', () => {
    const service: CandidateService = TestBed.get(CandidateService);
    expect(service).toBeTruthy();
  });
});
