import { Process } from './process';
import { Location } from './location';
import { Tech } from './tech';

export class Candidate {
  id: number;
  name: string;
  salary: number;
  locations: Location[];
  processes: Process[];
  skills: Tech[];

  processesAllCount = 0;
  processesAcceptedCount = 0;
  processesNewCount = 0;
  processesRejectedCount = 0;
}
