import { Position } from './position';

export class Process {
  id: number;
  position: Position;
  status: number;
  datAdded: string;
  skillScore: number;
  techStackScore: number;
  technologiesMatch: number;

  constructor(
    id?: number,
    position?: Position,
    status?: number,
    datAdded?: string,
    skillScore?: number,
    techStackScore?: number,
    technologiesMatch?: number) {
      this.id = id;
      this.position = position;
      this.status = status;
      this.datAdded = datAdded;
      this.skillScore = skillScore;
      this.techStackScore = techStackScore;
      this.technologiesMatch = technologiesMatch;

  }
}
