import { Location } from './location';
import { Tech } from './tech';

export class Position {
  id: number;
  title: string;
  companyName: string;
  techStack: number[];
  salary: number;
  location: Location;
  technologies: Tech[];

  constructor(
    id?: number,
    title?: string,
    companyName?: string,
    techStack?: number[],
    salary?: number,
    location?: Location,
    technologies?: Tech[],
    ) {
      this.id = id;
      this.title = title;
      this.companyName = companyName;
      this.techStack = techStack;
      this.salary = salary;
      this.location = location;
      this.technologies = technologies;

  }
}
