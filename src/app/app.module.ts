import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule} from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DahboardComponent } from './components/dahboard/dahboard.component';
import { ProcessItemComponent } from './components/process-item/process-item.component';
import { HeaderComponent } from './components/header/header.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import { ProcessListComponent } from './components/process-list/process-list.component';
import { Candidate } from './models/candidate';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { ROUTES } from './app.routes';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    DahboardComponent,
    ProcessItemComponent,
    HeaderComponent,
    ProcessListComponent,
    HomeComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    MatTabsModule,
    MatIconModule,
    DeviceDetectorModule.forRoot(),
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
    MatSidenavModule
  ],
  providers: [Candidate],
  bootstrap: [AppComponent]
})
export class AppModule { }
