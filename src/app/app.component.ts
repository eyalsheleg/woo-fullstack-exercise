import { Component, OnInit } from '@angular/core';
import { CandidateService } from './services/candidate.service';
import { Candidate } from './models/candidate';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'woo-ex';

  constructor(
    public candidateService: CandidateService,
    public candidate: Candidate,
    public deviceService: DeviceDetectorService) { }

  public ngOnInit() {
    this.candidateService.getDetails();
    console.log(this.candidate);
  }
}
