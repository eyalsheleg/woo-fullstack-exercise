import { DahboardComponent } from './components/dahboard/dahboard.component';
import {Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';

export const ROUTES: Routes = [
  {path: '', redirectTo: 'processes', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, pathMatch: 'full'},
  {path: 'profile', component: ProfileComponent, pathMatch: 'full'},
  {path: 'processes', component: DahboardComponent},
];
