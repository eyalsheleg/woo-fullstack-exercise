import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const fadeIn = trigger('fadeIn', [
    state('*', style({opacity: 0})),
    state('1', style({opacity: 1})),
    transition('* => 0', [
      // animate(1, style({ opacity: 0 }))
    ]),
    transition('0 => *', [
      animate(500, style({ opacity: 1 }))
    ])
]);
