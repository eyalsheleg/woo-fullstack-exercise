
export enum ProcessStatus {
  NEW = 1,
  ACCEPTED = 2,
  REJECTED = 3,
}
