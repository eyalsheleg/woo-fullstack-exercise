import { ProfileComponent } from './components/profile/profile.component';
import { MatIconModule } from '@angular/material/icon';
import { ProcessListComponent } from './components/process-list/process-list.component';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { DahboardComponent } from './components/dahboard/dahboard.component';
import { MatTabsModule } from '@angular/material/tabs';
import { Candidate } from './models/candidate';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ProcessItemComponent } from './components/process-item/process-item.component';
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { APP_BASE_HREF } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        DahboardComponent,
        ProcessListComponent,
        ProcessItemComponent,
        HomeComponent,
        ProfileComponent
      ],
      imports: [
        // BrowserModule,
        NoopAnimationsModule,
        MatTabsModule,
        MatIconModule,
        RouterModule.forRoot(ROUTES),
        MatSidenavModule
      ],
      providers: [Candidate, DeviceDetectorService, {provide: APP_BASE_HREF, useValue: '/'}],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'woo-ex'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('woo-ex');
  }));
});
