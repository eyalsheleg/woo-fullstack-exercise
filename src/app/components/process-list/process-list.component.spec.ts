import { CandidateService } from './../../services/candidate.service';
import { MatIconModule } from '@angular/material/icon';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessListComponent } from './process-list.component';
import { Candidate } from 'src/app/models/candidate';
import { ProcessItemComponent } from '../process-item/process-item.component';
import { DeviceDetectorService } from 'ngx-device-detector';

describe('ProcessListComponent', () => {
  let component: ProcessListComponent;
  let fixture: ComponentFixture<ProcessListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessListComponent, ProcessItemComponent ],
      providers: [Candidate, CandidateService, DeviceDetectorService],
      imports: [MatIconModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessListComponent);
    component = fixture.componentInstance;
    component.candidateService.getDetails();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
