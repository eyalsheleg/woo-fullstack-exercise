import { CandidateService } from './../../services/candidate.service';
import { Component, OnInit, Input } from '@angular/core';
import { Process } from 'src/app/models/process';
import { Candidate } from 'src/app/models/candidate';

@Component({
  selector: 'app-process-list',
  templateUrl: './process-list.component.html',
  styleUrls: ['./process-list.component.css'],
})
export class ProcessListComponent implements OnInit {
  @Input() type: number;


  public processes: Process[] = [];

  constructor(public candidate: Candidate, public candidateService: CandidateService) { }

  ngOnInit() {

  }
}
