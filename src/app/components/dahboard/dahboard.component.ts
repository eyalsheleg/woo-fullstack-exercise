import { Candidate } from './../../models/candidate';
import { Component, OnInit } from '@angular/core';
import { CandidateService } from 'src/app/services/candidate.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-dahboard',
  templateUrl: './dahboard.component.html',
  styleUrls: ['./dahboard.component.css']
})
export class DahboardComponent implements OnInit {

  constructor(public candidate: Candidate, public candidateService: CandidateService, public deviceService: DeviceDetectorService) { }

  ngOnInit() {
  }

}
