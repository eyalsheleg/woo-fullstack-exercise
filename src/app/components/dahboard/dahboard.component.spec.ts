import { ProcessItemComponent } from './../process-item/process-item.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DahboardComponent } from './dahboard.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { ProcessListComponent } from '../process-list/process-list.component';
import { Candidate } from 'src/app/models/candidate';
import { MatIconModule } from '@angular/material/icon';
import { CandidateService } from 'src/app/services/candidate.service';
import { DeviceDetectorService } from 'ngx-device-detector';

describe('DahboardComponent', () => {
  let component: DahboardComponent;
  let fixture: ComponentFixture<DahboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DahboardComponent, ProcessListComponent, ProcessItemComponent ],
      imports: [
        NoopAnimationsModule,
        MatTabsModule,
        MatIconModule
      ],
      providers: [Candidate, CandidateService, DeviceDetectorService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DahboardComponent);
    component = fixture.componentInstance;
    component.candidateService.getDetails();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
