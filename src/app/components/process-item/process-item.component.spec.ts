import { DeviceDetectorService } from 'ngx-device-detector';
import { CandidateService } from './../../services/candidate.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessItemComponent } from './process-item.component';
import { Position } from 'src/app/models/position';
import { Process } from 'src/app/models/process';
import { ProcessStatus } from 'src/app/enums/process-status.enum';
import { Location } from 'src/app/models/location';
import { Tech } from 'src/app/models/tech';
import { MatIconModule } from '@angular/material/icon';
import { Candidate } from 'src/app/models/candidate';

describe('ProcessItemComponent', () => {
  let component: ProcessItemComponent;
  let fixture: ComponentFixture<ProcessItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessItemComponent ],
      imports: [MatIconModule],
      providers: [Candidate, CandidateService, DeviceDetectorService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessItemComponent);
    component = fixture.componentInstance;
    component.candidateService.getDetails();
    component.process = new Process(1,
      new Position(1, 'Full Stack Engineer', 'Google', [1, 2, 3], 22000, new Location(2, 'Tel-Aviv'), [new Tech(1, 'Angular'), new Tech(2, 'Java'),]),
      ProcessStatus.ACCEPTED, '3 days ago', 75, 35);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
