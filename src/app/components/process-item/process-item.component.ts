import { CandidateService } from './../../services/candidate.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Process } from 'src/app/models/process';
import { ProcessStatus } from 'src/app/enums/process-status.enum';
import { Candidate } from 'src/app/models/candidate';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-process-item',
  templateUrl: './process-item.component.html',
  styleUrls: ['./process-item.component.css']
})
export class ProcessItemComponent implements OnInit {
  @Input() process: Process;

  public statusText: string;
  public techFulfilledCount = 0;

  public locationFulfilled: boolean;
  public sallaryFulfilled: boolean;
  public technologyFulfilled: boolean;


  constructor(private candidate: Candidate, public candidateService: CandidateService, public deviceService: DeviceDetectorService) { }

  ngOnInit() {
    this.sallaryFulfilled = this.process.position.salary >= this.candidate.salary;
    this.locationFulfilled = this.candidate.locations.filter(x => x.id === this.process.position.location.id).length > 0;

    this.process.position.technologies.forEach(tech => {
      if (this.candidate.skills.filter(x => x.id === tech.id).length > 0) {
        this.techFulfilledCount++;
      }
    });

    this.technologyFulfilled = this.techFulfilledCount >= (this.process.position.technologies.length / 2);
  }

  public acceptProcess() {
    this.process.status = ProcessStatus.ACCEPTED;
    this.candidateService.countProcesses();
  }

  public rejectProcess() {
    this.process.status = ProcessStatus.REJECTED;
    this.candidateService.countProcesses();
  }

  public isNew() {
    return this.process.status === ProcessStatus.NEW;
  }

}
